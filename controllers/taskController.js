//controllers contain the fuctions and business logic of our express js application
//meaning all the operation it can do will be placed in this file

//allows us to use the contents of the "task.js" file  in the models folder
const Task = require('../models/task');

//controller functionn for getting all the tasks
module.exports.getAllTask = () => {
    //the "return" statement, returns the result of the Mongoose method " find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
    //the ".then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client
    return Task.find({}).then(result => {
        //the "return" here, returns the result of the MongoDB query to the "result" parameter defined in the "then" method
        return result;
    })
}

//Controller function for creating task
//the request body coming form the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed 'requestBody' parameter in the controller file
module.exports.createTask = (requestBody) => {
    //create a task object based on the Mongoose model Task
    let newTask = new Task({
        name: requestBody.name
    })

    //return and .then palagi magkasama
    //saves the newly created "newTask" object in the MongoDB database
    //.then method waits until the task is stored/error
    //.then method will accept 2 arguments:
        //First parameter will store the result returne by the save method
        //Second parameter will tore the error object
    //Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter
    return newTask.save().then((task, error)=> {
        //if an error is encountered, the "return" statement will prevent any other line or code below is and within the same code block from executing
        //Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code.
        if(error){
            console.log(error);
            return false;
        }else{
            return task;
        }
    })
}

//Delete Task
//Business Logic
/**
 * 1. Look for the task with the corresponding id provided in the URL/route
 * 2. Delete the task using the Mongoose method "findByIdAndRemove" eith the same id provided in the route
 */
module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
        if(err){
            console.log(err)
            return false;
        }else{
            //delete successfull, returns the removed task object back to the client
            return removedTask;
        }
    })
}

//updating a task
/**
 * Business logic
 * 1. Get the task with id using the method "findById"
 * 2. Replace the task's name returned from the database with the "name" property from the request body
 * 3. SAve the task
 */
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if(error){
            console.log(err)
            return false;
        }
        //REsults of the "findById" method will be stored in the "result" parameter
        //Its "name" property will be reassigned the value of the "name" received from the request body
        result.name = newContent.name;

        return result.save().then((updateTask, saveErr) => {
            if(saveErr){
                console.log(err)
                return false;
            }else{
                //update successfully, returns the updated task object back to the client
                return updateTask;
            }
        })
    })
}

/**ACTIVITY */
//get Task
module.exports.getTask = (taskId) => {
    
    return Task.findById(taskId).then(result => {
        return result;
    })
}

//update status task
module.exports.updateStatus = (taskId, newStatus) => {
    return Task.findById(taskId).then((result, error) => {
        if(error){
            console.log(err)
            return false;
        }
        
        result.status = newStatus.status;

        return result.save().then((updateStatus, saveErr) => {
            if(saveErr){
                console.log(err)
                return false;
            }else{
                return updateStatus;
            }
        })
    })
}