//routes => containse all endpoint for our application
//we separate the routes such that "index.js" only contains information on the serve
//we need to use express Router() function to achieve this
const express = require('express');
//creates a router instance that functions as a minddleware and routing system
//allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();
//The "taskcontroller" allows us to use the functions defined in the taskControllers.js file
//require ang ginamit to connect to other files - eto pag import sa controller
const taskController = require('../controllers/taskController');
const task = require('../models/task');

//Routes are responsible for defining the URIs that our client access and the corresponding controller functions that will be used when a route is accessed.
//localhost:3001/tasks/getAll
router.get('/getAll', (req, res) => {
    //Invokes the "getAllTask" function from the "taskController.js" file and sends the result back to the client

    //"resultFromController" is only used here to make the code easier to understand but it's common practice to use the shorthand parameter name fr a result using the parameter name "result"/"res" 
    taskController.getAllTask().then(resultFromController => res.send(resultFromController));

})
//create a new task
router.post('/', (req, res) => {
    //createTask function needs the data from the request body, so we need to supply it to the dunction
    //if informtaion will be coming from the client side commonly form form, the data can be accessed form the request "body" property
    taskController.createTask(req.body).then(result => res.send(result));
})

//To delete a Task
//This route expects to receive a DELETE request at the URL "/task/:id"
//localhost:3001/tasks/:id
//the task ID is obtained from the URL is denoted by the ":id" identified in the route
//The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
//the word that comes after the colon(:) symbol will be the name of the URL parameter
//":id" is a WILDCARD where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
//EX
    //if the route is localhost:3000/tasks/1234
    //1234 is assigned to the "id" parameter in the URL
router.delete('/:id', (req, res)=> {
    //pag galing sa URL ung information, the data can be access from the request "params" property => req.params.id
    //in this case " id" is the name of the parameter, the property name of this object will match the given URL parameter
    //req.params.id 
    //pag sa body naman req.body
    taskController.deleteTask(req.params.id).then(result => res.send(result));
})

//update TASk
router.put('/:id', (req, res) => {
    //req.params.id retrieves the taskID from the parameter
    //req.body retrieves the data of the updates that will be applied to a task from the request body
    taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

/**ACTIVITY */
//get Task
router.get('/:id', (req, res) => {

    taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));

})

//update status
router.put('/:id/complete', (req, res) => {

    taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})

module.exports = router;