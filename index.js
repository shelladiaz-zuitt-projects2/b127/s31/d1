//setup dependencies
const express = require('express');
const mongoose = require('mongoose');

//this allows us to use all the routes defined in taskRoute.js
const taskRoute = require('./routes/taskRoute');

//server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended:true }));

//Database connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.dxzdo.mongodb.net/batch127_to-do?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true, //avoid any future errors when connecting mongodb
        useUnifiedTopology: true //avoid any future errors when connecting mongodb
    }
);

//connection error handling
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database"))


//add the task route
//allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use('/tasks', taskRoute);
//localhost:3001/tasks

app.listen(port, ()=> console.log(`Now listening to port ${port}`))


//Model -> Controllers -> routes -> index.js


/***
 * Activity:
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of s31 Activity.
11. Add the link in Boodle named Express Js Modules, Parameterized Routes.
 */